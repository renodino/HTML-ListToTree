# NAME

HTML::ListToTree - Convert nested HTML lists to Javascripted tree widget

# SYNOPSIS

        use HTML::ListToTree;
        #
        #       convert some arbitrary HTML from STDIN to a tree
        #
        $/ = undef;
        my $htmltree = <STDIN>;

        my $list2tree = HTML::ListToTree->new(
                Text => 'Some::Module',
                Link => '#TOP',
                Source => $htmltree);

        print $list2tree->render(
                CloseIcon => 'closedbook.gif',
                CSSPath =>'./css/dtree.css',
                IconPath => './img',
                JSPath => './js/dtree.js',
                OpenIcon => 'openbook.gif',
                RootIcon => 'globe.gif',
                Target => 'mainframe',
                Widget => 'HTML::ListToTree::DTree',
                Additions => '<small><i>Generated on July 1, 2007</i></small>'
                );
        #
        #       get the stuff we need for the tree
        #
        my $js = $list2tree->getJavascript();
        my $css = $list2tree->getCss();
        my $openimg = $list2tree->getIcon('openicon.gif');
        my %images = $list2tree->getIcons();
        #
        #       or just write it out
        #
        die $@
                unless $list2tree->writeJavascript() &&
                        $list2tree->writeCSS() &&
                        $list2tree->writeIcons();

# DESCRIPTION

Converts HTML nested lists (<ul> or <ol>) into an
HTML document containing an alternate widget using a specified
Widget class. By default, the Widget class is HTML::ListToTree::DTree, which
generates a Javascripted tree using the **dtree** widget _(with some minor modifications)_
available at [http://www.destroydrop.com/javascripts/tree/](http://www.destroydrop.com/javascripts/tree/).

# METHODS

### $list2tree = `HTML::ListToTree->new( %args )`

Convert the first instance of an HTML list (ordered or unordered)
in the supplied HTML text into a structure which can be used
to construct the alternate widget. Embedded links are preserved,
and nested lists are recursively traversed to construct expandable
tree nodes. If the first item in a nested list is another
nested list, the subordinate list is "pulled up" to the outer level
(in order to avoid unlabeled tree nodes).

On success, returns an HTML::ListToTree object

The method will `die` on any failure with an appropriate error message
in `$@`.

`%args` contains the following named parameters:

- Text => $text

    Text label for the node

- Link => $link

    Link URL for the node. If the link starts with '#', the link will be prefixed
    by a parent document URL.

- Children => \[ @nodes \] _(optional)_

    A list of HTML::ListToTree objects to populate the new node.
    Any objects extracted from a `Source` document are appended to this list.

- Source => $document _(optional)_

    A source document from which to extract HTML::ListToTree objects to populate the new node.
    Objects extracted from the document are appended to any supplied `Children` list.

- Widget => $widget _(optional)_

    Either the name of a Perl class, or a Perl object; default 'HTML::ListTiTree::DTree'.
    If a classname is provided, an instance of the class will be constructed. The class
    constructs a Javascript tree widget using the **dtree** library. The object provides methods
    for constructing the widget from HTML lists members. See [HTML::ListToTree::DTree](https://metacpan.org/pod/HTML::ListToTree::DTree)
    for methods the widget class should implement.

### @nodes = `$list2tree->addChildren( @nodes )`

Add the specified list of either HTML::ListToTree objects, or
nodes created from 2-tuples of Text => Link values, to the object's list
of children. Returns the list of added nodes. E.g.,

        @nodes = $list2tree->addChildren(
                'Chapter 1' => '#chapter1',
                $chapter2_node,
                'Chapter 3' => '#chapter3',
                );

### @nodes = `$list2tree->addFromDocument( $html )`

Add a list of nodes extracted from the supplied document to the object's
list of children. Returns the list of extracted nodes. Applies the
object's [extractTree()](https://metacpan.org/pod/extractTree\(\)) method to the supplied document.

### @nodes = `$list2tree->getChildren()`

Returns the list of child nodes of the object.

### $text = `$list2tree->getText()`

Returns the text label of the object.

### `$list2tree->setText( $text )`

Sets the text label of the object.
Returns the object.

### $link = `$list2tree->getLink()`

Returns the link URL of the object.

### `$list2tree->setLink( $link )`

Sets the link URL of the object.
Returns the object.

### $node = `$list2tree->getNodeByText( $text )`

Search the object's children for the first node
with the specified text label. The search is breadth-first,
i.e., all siblings are searched before any children are searched.
Returns the matching node if a match is found, otherwise,
returns undef.

### $node = `$list2tree->getNodeByLink( $link )`

Search the object's children for the first node
with the specified link URL. The search is breadth-first,
i.e., all siblings are searched before any children are searched.
Returns the matching node if a match is found, otherwise,
returns undef.

### $tree = `$list2tree->extractTree( $html )`

Extracts a HTML::ListToTree from the first top-level
list element in supplied document. This method is usually
not used by applications, but can be overridden by
subclasses to provide alternate extraction logic.

### $widget = `$list2tree->render( %args )`

Render an HTML document containing the widget for the object's tree. 
The resulting document is suitable for use with an HTML frameset document. Subclasses
may choose to override this method to provide alternate
renderings of the tree. `%args` contains the following named parameters:

- **Additions**

    _(optional)_ HTML text to be appended to the generated tree for display
    in the same frame as the navigation tree. Default none.

- **CloseIcon**

    _(optional)_ Name of icon file used for closed tree nodes; default 'closedbook.gif'

- **CSSPath**

    _(optional)_ Path to the stylesheet file dtree.css used by dtree; default './css'

- **IconPath**

    _(optional)_ Path to the location of icons used by dtree; default './img'

- **JSPath**

    _(optional)_ Path to the Javascript file dtree.js; default '.js'

- **UseIcons**

    _(optional)_ If true (the default), tree nodes are decorated with icons.

- **OpenIcon**

    _(optional)_ Name of icon file used for open tree nodes; default 'openbook.gif'

- **RootIcon**

    _(optional)_ Name of icon file used for the root tree node; default is same as OpenIcon

- **Target**

    _(optional)_ Name of an HTML frame to contain the document being navigated; default 'mainframe'

### $js = `$list2tree->getJavascript()`

Returns any Javascript library required for the widget.

### $css = `$list2tree->getCSS()`

Returns any CSS stylesheet required for the widget.

### $image = `$list2tree->getIcon( $name )`

Returns the binary image data for the widget icon specified by `$name`.

### %images = `$list2tree->getIcons()`

Returns the binary image data for all the icons provided by the widget
as a hash mapping the image name to its binary data.

# SUBCLASSING

This module was developed primarily to aid in beautifying the
output of [Pod::ProjectDocs](https://metacpan.org/pod/Pod::ProjectDocs). Therefore, the provided
`extractTree()` implementation may not be suitable for
some applications. Likewise, some applications may wish to
render the tree differently, or use a different Javascript widget,
in which case the provided `render()` implementation may not be appropriate.

Both of those situations can be remedied by simply subclassing HTML::ListToTree
to override the `extractTree()` and/or `render()` methods. Refer
to the source code for details on the structure of the nodes generated
by `extractTree()` and consumed by `render()`.

Also, the provided default dtree browser widget (provided within
HTML::ListToTree::DTree) can be overridden by specifying an
alternate Perl package name as the `Widget` constructor argument.

## Widget Package Methods

The widget package must provide the following methods:

### new()

Constructor.

### start( %args )

Starts the construction of the widget. The contents of `%args` depends on the
widget class; for the default supports the following members:

- **RootText** 

    text for root node of tree

- **CSSPath** 

    full path to the stylesheet; default './css/dtree.css'

- **JSPath** 

    full path to the Javascript document; default './js/dtree.js'

- **UseIcons** 

    if true, widget includes open/close icons; default false

- **RootIcon** 

    icon used for root node of tree; default none

- **RootLink** 

    URL of root node of tree; default none

- **Target** 

    name of target frem for tree links; default 'mainframe'

- **CloseIcon** 

    icon used for closed nodes; default <IconPath>/folder.gif

- **OpenIcon** 

    icon used for open node; default <IconPath>/folderopen.gif

- **IconPath** 

    path to the icon image directory

### addLeaf( $node, $parent, $text, $link )

Add a leaf node to the widget. 

- `$node` is a unique integer node id.
- `$parent` is the unique node id of the parent node. 
- `$text` is the text label for the node. 
- `$link` is a URL for the content the node should cause
to be presented in the target frame when the node is clicked.

### add( $node, $parent, $text, $link )

Add an intermediate node to the widget. The parameters are the
same as for `addLeaf()`.

### getJavascript()

Returns the text of any Javascript library required for the widget

### getCSS()

Returns the text of any stylesheet required for the widget

### getIcon( $name )

Returns the binary image data for the specified widget icon.
Widget classes are expected to provide their own icons encoded
within their package (usually via [MIME::Base64](https://metacpan.org/pod/MIME::Base64)).

### getIcons()

Returns the image data for all widget icons as a hash mapping
icon names to binary image data.
Widget classes are expected to provide their own icons encoded
within their package (usually via [MIME::Base64](https://metacpan.org/pod/MIME::Base64)).

### writeJavascript( \[ $path \] )

Writes a file of the text of any Javascript library required for the widget. If
`$path` is not specified, uses the JSPath specified for `render()`.

### getCSS( \[ $path \] )

Writes a file of the text of any stylesheet required for the widget. If
`$path` is not specified, uses the CSSPath specified for `render()`.

### writeIcon( $name \[ , $path \] )

Writes a file of the binary image data for the specified widget icon.
Widget classes are expected to provide their own icons encoded
within their package (usually via [MIME::Base64](https://metacpan.org/pod/MIME::Base64)).
If `$path` is not specified, uses the IconPath specified for `render()`.

### writeIcons( \[ $path \] )

Writes files of the image data for all widget icons.
Widget classes are expected to provide their own icons encoded
within their package (usually via [MIME::Base64](https://metacpan.org/pod/MIME::Base64)).
If `$path` is not specified, uses the IconPath specified for `render()`.

# PREREQUISITES

Perl 5.8 or later.

[HTML::TreeBuilder](https://metacpan.org/pod/HTML::TreeBuilder)

**dtree** from [http://www.destroydrop.com/javascripts/tree/](http://www.destroydrop.com/javascripts/tree/);
(a copy of the package, along with associated images and CSS,
is included in the [HTML::ListToTree::DTree](https://metacpan.org/pod/HTML::ListToTree::DTree) package)

# SEE ALSO

[index2dtree](https://metacpan.org/pod/index2dtree) - included script to perform conversions from the command line

[HTML::Widgets::NavMenu](https://metacpan.org/pod/HTML::Widgets::NavMenu) - a similar solution, but for pure HTML (no Javscript)

[CGI::Explorer](https://metacpan.org/pod/CGI::Explorer) - similar solution, but limited to Windows(R) Explorer look alike
(no configurable icons)

[Tree::Simple::View](https://metacpan.org/pod/Tree::Simple::View) - a generalized solution for creating views of Tree::Simple
implementations

# AUTHOR, COPYRIGHT, and LICENSE

Copyright (C) 2007, Dean Arnold, Presicient Corp., USA. All rights reserved.

Excluding the dtree widget software and components included in the 
[HTML::ListToTree::DTree](https://metacpan.org/pod/HTML::ListToTree::DTree) package, permission is granted to use this software 
under the same terms as Perl itself. Refer to the [Perl Artistic License](https://metacpan.org/pod/perlartistic) for details.

The dtree widget software and components included in the 
[HTML::ListToTree::DTree](https://metacpan.org/pod/HTML::ListToTree::DTree) package are distributed in accordance
with the following license and copyright:

        /*--------------------------------------------------|
        | dTree 2.05| www.destroydrop.com/javascripts/tree/ |
        |---------------------------------------------------|
        | Copyright (c) 2002-2003 Geir Landrö               |
        |                                                   |
        | This script can be used freely as long as all     |
        | copyright messages are intact.                    |
        |                                                   |
        | Updated: 17.04.2003                               |
        |--------------------------------------------------*/
